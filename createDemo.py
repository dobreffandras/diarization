import os
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("inputVideo", help="Input tracked video file")
parser.add_argument("-a", "--audio", help="Original audio track", default="vad.wav")
parser.add_argument("-fps", "--fps", help="fps of the created video", default=30, type=int)
parser.add_argument("--width", help="width of the video", default=30, type=int)
args = parser.parse_args()

speech = "speech.txt"
filteredData = "filteredData.txt"

demo_speech = "demo_speech.mp4"
demo_filtered = "demo_filtered.mp4"

pixelwidth = args.width
vad = "vad.mp4"

temp_with_speech = "temp_with_speech.mp4"
temp_with_speech_and_vad = "temp_with_speech_and_vad.mp4"

filtered_voice = "filtered_voice.wav"
output_speech = "output_speech.mp4"

#'''Common
cmd_vad = "python %s\VAD.py -a %s" % (os.path.dirname(__file__), args.audio) # create vad demo footage
os.system(cmd_vad)
#'''

#'''Speech
cmd_speech_demo = "python %s\createVideo.py %s %s %s %d -fps %d" % (os.path.dirname(__file__), speech, filteredData, demo_speech, pixelwidth, args.fps) # create speech demo footage
cmd_temp_with_speech = "ffmpeg -y -i %s -i %s -filter_complex vstack %s" % (args.inputVideo, demo_speech, temp_with_speech) # create temp1 
cmd_temp_with_speech_and_vad = "ffmpeg -y -i %s -i %s -filter_complex vstack %s" % (temp_with_speech, vad, temp_with_speech_and_vad) # create temp2
cmd_add_sound_speech = "ffmpeg -y -i %s -i %s -map 0:v:0 -map 1:a:0 %s" % (temp_with_speech_and_vad, filtered_voice, output_speech) # add sound

os.system(cmd_speech_demo)
os.system(cmd_temp_with_speech)
os.system(cmd_temp_with_speech_and_vad)
os.system(cmd_add_sound_speech)
#'''
