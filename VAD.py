from diarization.voicing.audio_tools import aeDetect
from scipy.io import wavfile
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-a", "--audio", help="Voice activated audio", default="vad.wav")
parser.add_argument("-dpi", "--dpi", help="dpi of the plotted data", default=100.0, type=float)
parser.add_argument("-fps", "--fps", help="FPS of the created video", default=30, type=int)
parser.add_argument("-width", "--width", help="Width of the created video", default=960, type=int)
args = parser.parse_args()

dpi = args.dpi
FPS = args.fps
width = args.width
  
def ani_frame(samples, samplesRate, FPS, width):
    fig, ax = plt.subplots(figsize=(width/dpi, 2))

    x = np.arange(0, wavSampleRate, 1)
    minVal = np.min(samples)
    maxVal = np.max(samples)
    line, = ax.plot(x, [1]*len(x))
    ax.set_ylim([minVal, maxVal])

    def animate(i):
        currentData = samples[i:i+wavSampleRate]
        line.set_ydata(currentData)  # update the data
        #print "block [%d - %d]" % (i, i+wavSampleRate)
        return line,


    # Init only required for blitting to give a clean slate.
    def init():
        line.set_ydata(np.ma.array(x, mask=True))
        return line,

    ani = animation.FuncAnimation(fig, animate, np.arange(1, len(samples)-wavSampleRate, wavSampleRate/FPS), init_func=init,
                                  interval=FPS, blit=True)
    #plt.show()
    writer = animation.writers['ffmpeg'](fps=FPS)
    print "saving"
    ani.save('vad.mp4',writer=writer,dpi=dpi)
    return ani    

wavSampleRate, wavData = wavfile.read(args.audio)

ani_frame(wavData, wavSampleRate, FPS, width)

#wavfile.write("audio_vad.wav", wavSampleRate, wavData)