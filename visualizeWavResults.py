import os
import argparse
import glob
import sys

parser = argparse.ArgumentParser()
parser.add_argument("inputFolder")
args = parser.parse_args()

CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))

files = "*.wav"
if os.path.isdir(args.inputFolder):
    strFilePattern = os.path.join(args.inputFolder, files)
else:
    strFilePattern = args.inputFolder + files

wavFilesList = []
wavFilesList.extend(glob.glob(strFilePattern))
wavFilesList = sorted(wavFilesList)
if len(wavFilesList) == 0:
    print "No WAV files found!"
else:
    #print wavFilesList

    for wavFile in wavFilesList:
        os.system("python %s/plotWavFile.py %s" %(CURRENT_PATH, wavFile))
    