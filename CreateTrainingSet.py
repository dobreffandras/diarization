import argparse
from diarization.cutUpVideo import cutUpVideo
import os

parser = argparse.ArgumentParser()
parser.add_argument("sourceVideo", help="The file containing the raw lowermouth data")
parser.add_argument("fps", help="The fps of the data", type=float)
parser.add_argument("--chunksFile",  help="File of chunks", default=None)
parser.add_argument("--verbose", help = "Creates additional files holding partial results", action="store_true")
args = parser.parse_args()

CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))
#print CURRENT_PATH

def createTrainingSetForOneVideo(videoFile, audioFile, lowerFile, upperFile, fps):
    if args.verbose:
        outputVideo = str(os.path.splitext(os.path.basename(videoFile))[0])+"_tracked.avi"
        trackCommand = CURRENT_PATH + "\diarization\libfacetracker-video.exe %s %s %s %s" % (videoFile, lowerFile, upperFile, outputVideo)
    else:
        trackCommand = CURRENT_PATH + "\diarization\libfacetracker-video.exe %s %s %s" % (videoFile, lowerFile, upperFile)
    os.system(trackCommand)
    createAudio = "ffmpeg -i %s -ar 16k -ac 1 -acodec pcm_s16le %s" % (videoFile, audioFile)
    os.system(createAudio)
    diarization_cmd = "python "+ CURRENT_PATH + ".\diarization\main.py %s %s %s %d" % (lowerFile, upperFile, audioFile, fps)
    if args.verbose:
        diarization_cmd = diarization_cmd + " --verbose"
    os.system(diarization_cmd)

if not args.chunksFile == None:
    segmentsPrefix = "output"
    cutUpVideo(args.sourceVideo, args.chunksFile, segmentsPrefix)

    file = open(args.chunksFile,"r")
    segmentsCount = len(file.readlines())
    
    for i in range(0, segmentsCount):
        video = segmentsPrefix+str(i)+".mp4"
        audio = segmentsPrefix+str(i)+".wav"
        lower = segmentsPrefix+str(i)+"_lower.txt"
        upper = segmentsPrefix+str(i)+"_upper.txt"
        createTrainingSetForOneVideo(video, audio, lower, upper, args.fps)
else:
    video = args.sourceVideo
    audio = video + ".wav"
    lower = video + "_lower.txt"
    upper = video + "_upper.txt"
    createTrainingSetForOneVideo(video, audio, lower, upper, args.fps)
