import numpy as np
import cv2
import math
from diarization.classification.classify import classifyRawAudioDataWithSVM
import os
import argparse

def writeFrameOfVideo(videoWriter, frame, name, strength, score):
    cv2.rectangle(frame, (100,75), (350,110), (0,0,0), thickness=cv2.cv.CV_FILLED, shift=0)
    cv2.putText(frame,name,(100,100), cv2.FONT_HERSHEY_SIMPLEX, 1 ,(255,64,64), 2)
    
    #cv2.putText(frame,'Strength: %d' % (strength),(100,140), cv2.FONT_HERSHEY_SIMPLEX, 1 ,(255,64,64), 2)
    #cv2.putText(frame,'Score: %d' % (score),(100,190), cv2.FONT_HERSHEY_SIMPLEX, 1 ,(255,64,64), 2)
    videoWriter.write(frame)

def writeStrengthOnVideo(inputVideoName, outPutVideoName, strengthArray, framesPerStrength, fps, width, height): 
    cap = cv2.VideoCapture(inputVideoName)

    # Define the codec and create VideoWriter object
    fourcc = cv2.cv.CV_FOURCC(*'XVID')
    out = cv2.VideoWriter(outPutVideoName,fourcc, fps, (width,height))

    frameidx = 0
    while(cap.isOpened()):
        print "frame#%d" % (frameidx)
        ret, frame = cap.read()
        if ret==True:
            strengthidx = int(math.floor(frameidx/framesPerStrength))
            if not strengthidx < len(strengthArray):
                break
            name = strengthArray[strengthidx][0]
            strength = strengthArray[strengthidx][1]
            score = strengthArray[strengthidx][2]
            if strength > 0:
                writeFrameOfVideo(out, frame, name, strength, score)
            else:
                writeFrameOfVideo(out, frame, "No speaker", 0, 0)
            frameidx = frameidx+1
        else:
            break
        
    cap.release()
    out.release()
    cv2.destroyAllWindows()
    
parser = argparse.ArgumentParser()
parser.add_argument("inputVideo", help="The file containing the raw lowermouth data")
parser.add_argument("outputVideo",  help="File of chunks")
parser.add_argument("fps", help="The fps of the data", type=float)
parser.add_argument("model", help="The file containing the raw lowermouth data")
parser.add_argument("--width", help="The width", default=1280, type=int)
parser.add_argument("--height", help="The height", default=720, type=int)
args = parser.parse_args()

audio = "audio.wav"
ffmpeg_cmd = "ffmpeg -i %s -ar %s -ac %d %s" % (args.inputVideo, "16K", 1, audio)
os.system(ffmpeg_cmd)

strengthList = classifyRawAudioDataWithSVM(audio, int(16000/3), args.model)
print strengthList
writeStrengthOnVideo(args.inputVideo, 'tmp.avi', strengthList, args.fps/3, args.fps, args.width, args.height)

ffmpeg_cmd = "ffmpeg -i tmp.avi -i %s -map 0:v:0 -map 1:a:0 %s" % (audio, args.outputVideo)

os.system(ffmpeg_cmd)