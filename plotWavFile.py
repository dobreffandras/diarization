import numpy as np
import matplotlib.pyplot as plt
import sys
from scipy.io import wavfile

filepath = sys.argv[1]

wavSampleRate, data = wavfile.read(filepath)
wavData = data

wavfile.write("asd.wav", wavSampleRate, wavData)

t = np.linspace(0, len(wavData), len(wavData))
plt.figure(num=None, figsize=(len(wavData)*50/wavSampleRate, 6), dpi=350)
plt.plot(t, wavData, 'b-')
plt.savefig(filepath+".png")
plt.close()