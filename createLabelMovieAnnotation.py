import argparse
import os 
from pyAudioAnalysis import audioTrainTest as aT
from diarization.classification.classify import classifyRawAudioDataWithSVM
from scipy.io import wavfile
import xml.etree.ElementTree as ET
import xml.dom.minidom
import sys
from xml.dom import minidom
from random import randint

parser = argparse.ArgumentParser()
parser.add_argument("video", help="The video to be annotated")
parser.add_argument("fps", help="The fps of the video", type=float)
parser.add_argument("--audio", help="The audio channel of the video to be annotated")
parser.add_argument("--modelType", help="The type of the given model", default="svm")
parser.add_argument("model", help="The model to be tested")
args = parser.parse_args()

def addLabel(labels, index, name, windowsize):
    if not name in labels:
        labels[name] = []
    labelsArrayForName = labels[name]
    labelsArrayForName.append((int(index), int(index+windowsize)))

def createTracksAndLabelSet(audioFile, fps, model):
    tracks = ET.Element("tracks")
    labelSet = ET.Element("labelSet")
    sampleRate, _ = wavfile.read(audioFile)
    framesPerStrength = fps/3
    resultSet = classifyRawAudioDataWithSVM(audioFile,int(sampleRate/3),args.model)
    labels = {}
    for resultidx, result in enumerate(resultSet):
        name = result[0]
        strength = result[1]
        score = result[2]
        if strength > 0:
            addLabel(labels, resultidx*framesPerStrength, name, framesPerStrength)
    for key, value in labels.iteritems():
        track = ET.SubElement(tracks, "track", {"name":key, "type":key})
        ET.SubElement(labelSet, "labelType", {"name" : key, "blue": str(randint(0,255)), "green" : str(randint(0,255)), "red" : str(randint(0,255))})
        for label in value:
            ET.SubElement(track, "label", {"start" : str(label[0]), "end": str(label[1])})
    return tracks, labelSet

audioFile = args.audio
videoFile = args.video

if audioFile is None:
    audioFile = "audio.wav"
    createAudio = "ffmpeg -i %s -ar 16k -ac 1 -acodec pcm_s16le %s" % (videoFile, audioFile)
    os.system(createAudio)
    
videopath = os.path.abspath(videoFile)

tracks, labelSet = createTracksAndLabelSet(audioFile, args.fps, args.model)

videos = ET.Element("videos")
ET.SubElement(videos, "video", {"path" : videopath})

annotation = ET.Element("annotation")
annotation.append(videos)
annotation.append(labelSet)
annotation.append(tracks)

def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

print prettify(annotation)