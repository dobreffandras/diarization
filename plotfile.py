import numpy as np
import matplotlib.pyplot as plt
import sys

filepath = sys.argv[1]

file = open(filepath,"r")
array = [[float(x) for x in line.split()] for line in file]
t = np.linspace(0, len(array[0]), len(array[0]))
plt.figure(num=None, figsize=(len(array[0])/30, 6), dpi=350)
plt.plot(t, array[0], 'b-')
plt.savefig(filepath+".png")
plt.close()