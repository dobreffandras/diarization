import numpy as np
import matplotlib.pyplot as plt
import argparse
import matplotlib.animation as animation


parser = argparse.ArgumentParser()
parser.add_argument("speechinputfile", help="The file from the data is read")
parser.add_argument("filteredinputfile", help="The file from the data is read")
parser.add_argument("outputfile", help="The file to write")
parser.add_argument("width", help="Width of the video in pixel", type=int)
parser.add_argument("-dpi", "--dpi", help="dpi of the created video", type=float, default=100.0)
parser.add_argument("-fps", "--fps", help="fps of the created video", type=int, default=30)

args = parser.parse_args()

dpi = args.dpi
FPS = args.fps
  

def ani_frame(speech_samples, filtered_samples, samplesRate, FPS, startFrame, endFrame, width):
    fig = plt.figure(figsize=(width/dpi, 2))
    ax = plt.axes(xlim = (0, FPS), ylim = (-10,10))

    speech_samples = map(lambda x: x*5, speech_samples)
    
    N = 2
    lines = [plt.plot([], [])[0] for _ in range(N)]

    def animate(i):
        lines[0].set_data(np.arange(0,FPS), speech_samples[i:i+FPS])
        lines[1].set_data(np.arange(0,FPS), filtered_samples[i:i+FPS])
        return lines


    # Init only required for blitting to give a clean slate.
    def init():
        for line in lines:
            line.set_data([], [])
        return lines

    ani = animation.FuncAnimation(fig, animate, np.arange(startFrame, endFrame), init_func=init, blit=True)
    #plt.show()
    writer = animation.writers['ffmpeg'](fps=FPS)
    print "saving"
    ani.save(args.outputfile,writer=writer,dpi=dpi)
    return ani

#speechinput
speechinputfilepath = args.speechinputfile

speechinputfile = open(speechinputfilepath,"r")
array1 = [[float(x) for x in line.split()] for line in speechinputfile]
speech_samples = array1[0]+([0] *FPS)

#filteredinputfile
filteredinputfilepath = args.filteredinputfile

filteredinputfile = open(filteredinputfilepath,"r")
array2 = [[float(x) for x in line.split()] for line in filteredinputfile]
filtered_samples = array2[0]+([0] *FPS)

ani_frame(speech_samples, filtered_samples, FPS, FPS, 1, len(speech_samples)-FPS, args.width)