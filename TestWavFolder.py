import os
import glob
import sys
from pyAudioAnalysis import audioTrainTest as aT
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("folderName", help="The file containing the raw lowermouth data")
parser.add_argument("modelType", help="The file containing the raw lowermouth data")
parser.add_argument("model", help="The file containing the raw lowermouth data")
args = parser.parse_args()

def classifyFolderWrapper(inputFolder, modelType, modelName):
    if not os.path.isfile(modelName):
        raise Exception("Input modelName not found!")

    files = "*.wav"
    if os.path.isdir(inputFolder):
        strFilePattern = os.path.join(inputFolder, files)
    else:
        strFilePattern = inputFolder + files

    wavFilesList = []
    wavFilesList.extend(glob.glob(strFilePattern))
    wavFilesList = sorted(wavFilesList)
    if len(wavFilesList) == 0:
        print "No WAV files found!"
        return

    [_, _, classNames] = aT.fileClassification(wavFilesList[0], modelName, modelType)
    
    print(';'.join('{}'.format(val) for k, val in enumerate(classNames)))
    for wavFile in wavFilesList:
        [Result, P, _] = aT.fileClassification(wavFile, modelName, modelType)
        print(';'.join('{:0.3f}'.format(val).replace('.', ',') for k, val in enumerate(P)))
        '''
        for i, className in enumerate(classNames):
            sys.stdout.write("%.03f, " % (P[i]))
        sys.stdout.write("\n")
        sys.stdout.flush()
        '''
classifyFolderWrapper(args.folderName, args.modelType, args.model)