import numpy as np

def zeroRate(data):
    count = 0
    for i in range(0, len(data)):
        if data[i] == 0:
            count = count + 1
    rate = float(count) / float(len(data))
    print rate
    return rate


def create_wav_chunks(filtered_wav_data, windowsize):
    last_index = len(filtered_wav_data)
    indeces = range(0, last_index, windowsize)
    print indeces
    chunks = []
    for i in indeces:
        chunk = filtered_wav_data[i:i + windowsize]
        if zeroRate(chunk) < 0.2:
            chunks.append(chunk)

    return chunks