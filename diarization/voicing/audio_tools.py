import matplotlib
matplotlib.use('Agg') # uncomment lines for no-display environments

#
# A module containing part of the audio_tools.py
#    see http://codegists.com/snippet/python/audio_toolspy_kastnerkyle_python
# plus:
#    AED based VAD: https://github.com/antoniorohit/CS289A/blob/master/Final%20Project/VAD/aed.py
#

import sys
sys.path.append("..")

import numpy as np
import scipy
import scipy.signal as sg
from scipy.io import wavfile
import os

class VAD(object):
    def __init__(self, fs, markov_params=(0.5, 0.1), alpha=0.99, NFFT=2048,
                 n_iters=10, win_size_sec=0.05, win_hop_sec=0.025,
                 max_est_iter=-1, epsilon=1e-6):
        """ Voice Activity Detection
        Parameters
        ----------
        fs : int
            sampling rate
        markov_params : (int, int)
            parameters for the hangover scheme
        alpha : float
            SNR estimate coefficient
        NFFT : int
            size of the FFT
        n_iters : int
            number of iterations in noise estimation
        win_size_sec : float
            window size in seconds
        win_hop_sec : float
            window shift in seconds
        epsilon : float
            convergence threshold
        """
        self.fs = fs
        self.a01, self.a10 = markov_params
        self.a00 = 1 - self.a01
        self.a11 = 1 - self.a10
        self.alpha = alpha
        self.NFFT = NFFT
        self.win_size_sec = win_size_sec
        self.win_hop_sec = win_hop_sec
        self.epsilon = epsilon
        self.n_iters = n_iters
        self.max_est_iter = max_est_iter

        self.wlen = int(fs * self.win_size_sec)
        self.fshift = int(fs * self.win_hop_sec)
        self.win = hamming(self.wlen)

    def detect_speech(self, sig, threshold, n_noise_frames=20):
        """
        Return binary speech/non-speech decisions for each frame.
        Parameters
        ----------
        sig : ndarray
            audio signal
        threshold : float
            decision threshold
        n_noise_frames : int
            number of frames at start of file to use for initial noise model
        """
        return self.activations(sig, n_noise_frames) > threshold

    def stft(self, sig):
        """
        Short term fourier transform.
        Parameters
        ----------
        sig : ndarray
            signal
        Returns
        -------
        windowed fourier transformed signal
        """
        s = np.pad(sig, (self.wlen//2, 0), 'constant')
        cols = np.ceil((s.shape[0] - self.wlen) / self.fshift + 1)
        s = np.pad(s, (0, self.wlen), 'constant')
        frames = as_strided(s, shape=(cols, self.wlen),
                            strides=(s.strides[0]*self.fshift,
                                     s.strides[0])).copy()
        return np.fft.rfft(frames*self.win, self.NFFT)

    def activations(self, sig, n_noise_frames=20):
        """
        Returns continuous activations of the voice activity detector.
        Parameters
        ----------
        sig : ndarray
            audio signal
        n_noise_frames : int
            number of frames at start of file to use for initial noise model
        """
        frames = self.stft(sig)
        n_frames = frames.shape[0]

        noise_var_tmp = zeros(self.NFFT//2+1)
        for n in xrange(n_noise_frames):
            frame = frames[n]
            noise_var_tmp = noise_var_tmp + (conj(frame) * frame).real

        noise_var_orig = noise_var_tmp / n_noise_frames
        noise_var_old = noise_var_orig

        G_old = 1
        A_MMSE = zeros((self.NFFT//2+1, n_frames))
        G_MMSE = zeros((self.NFFT//2+1, n_frames))

        cum_Lambda = zeros(n_frames)
        for n in xrange(n_frames):
            frame = frames[n]
            frame_var = (conj(frame) * frame).real

            noise_var = noise_var_orig

            if self.max_est_iter == -1 or n < self.max_est_iter:
                noise_var_prev = noise_var_orig
                for iter_idx in xrange(self.n_iters):
                    gamma = frame_var / noise_var
                    Y_mag = np.abs(frame)

                    if n:
                        xi = (self.alpha *
                              ((A_MMSE[:, n-1]**2 / noise_var_old) +
                               (1 - self.alpha) * maximum(gamma - 1, 0)))
                    else:
                        xi = (self.alpha +
                              (1 - self.alpha) * maximum(gamma - 1, 0))
                    v = xi * gamma / (1 + xi)
                    bessel_1 = i1(v/2)
                    bessel_0 = i0(v/2)
                    g_upd = (sqrt(pi) / 2) * (sqrt(v) / gamma) * np.exp(v/-2) * \
                        ((1 + v) * bessel_0 + v * bessel_1)
                    np.putmask(g_upd, np.logical_not(np.isfinite(g_upd)), 1.)
                    G_MMSE[:, n] = g_upd
                    A_MMSE[:, n] = G_MMSE[:, n] * Y_mag

                    gamma_term = gamma * xi / (1 + xi)
                    gamma_term = minimum(gamma_term, 1e-2)
                    Lambda_mean = (1 / (1 + xi) + exp(gamma_term)).mean()

                    weight = Lambda_mean / (1 + Lambda_mean)
                    if isnan(weight):
                        weight = 1

                    noise_var = \
                        weight * noise_var_orig + (1 - weight) * frame_var

                    diff = np.abs(np.sum(noise_var - noise_var_prev))
                    if diff < self.epsilon:
                        break
                    noise_var_prev = noise_var

            gamma = frame_var / noise_var
            Y_mag = np.abs(frame)

            if n:
                xi = self.alpha * ((A_MMSE[:, n-1]**2 / noise_var_old) +
                                   (1 - self.alpha) * maximum(gamma - 1, 0))
            else:
                xi = self.alpha + (1 - self.alpha) * maximum(gamma - 1, 0)

            v = (xi * gamma) / (1 + xi)
            bessel_0 = i0(v/2)
            bessel_1 = i1(v/2)
            g_upd = (sqrt(pi) / 2) * (sqrt(v) / gamma) * \
                exp(v/-2) * ((1 + v) * bessel_0 + v * bessel_1)
            np.putmask(g_upd, np.logical_not(np.isfinite(g_upd)), 1.)
            G_MMSE[:, n] = g_upd
            A_MMSE[:, n] = G_MMSE[:, n] * Y_mag

            Lambda_mean = (log(1/(1+xi)) + gamma * xi / (1 + xi)).mean()

            G = ((self.a01 + self.a11 * G_old) /
                 (self.a00 + self.a10 * G_old) * Lambda_mean)

            cum_Lambda[n] = G

            G_old = G
            noise_var_old = noise_var
        return cum_Lambda

#
# WARNING! #fix branches were added later to prevent errors in extreme cases;
#            experiments have shown that during a 45k window wav file only ~20 iterations of the loop entered these branches
def voiced_unvoiced(X, corr_limit=0.3, window_size=256, window_step=128, copy=True):
    """
    Voiced unvoiced detection from a raw signal
 
    Based on code from:
        https://www.clear.rice.edu/elec532/PROJECTS96/lpc/code.html
 
    Other references:
        http://www.seas.ucla.edu/spapl/code/harmfreq_MOLRT_VAD.m
 
    Parameters
    ----------
    X : ndarray
        Raw input signal
 
    window_size : int, optional (default=256)
        The window size to use, in samples.
 
    window_step : int, optional (default=128)
        How far the window steps after each calculation, in samples.
 
    copy : bool, optional (default=True)
        Whether to make a copy of the input array or allow in place changes.
    """
    X = np.array(X, copy=copy)
    if len(X.shape) < 2:
        X = X[None]
    n_points = X.shape[1]
    n_windows = n_points // window_step
    # Padding
    pad_sizes = [(window_size - window_step) // 2,
                 window_size - window_step // 2]
    # TODO: Handling for odd window sizes / steps
    X = np.hstack((np.zeros((X.shape[0], pad_sizes[0])), X,
                   np.zeros((X.shape[0], pad_sizes[1]))))
 
    clipping_factor = 0.6
    b, a = sg.butter(10, np.pi * 9 / 40)
    voiced_unvoiced = np.zeros((n_windows, 1))
    period = np.zeros((n_windows, 1))
    for window in range(max(n_windows - 1, 1)):
        XX = X.ravel()[window * window_step + np.arange(window_size)]
        XX *= sg.hamming(len(XX))
        XX = sg.lfilter(b, a, XX)
        left_max = np.amax(np.abs(XX[:len(XX) // 3]))
        right_max = np.amax(np.abs(XX[-len(XX) // 3:]))
        clip_value = clipping_factor * np.min([left_max, right_max])    # 0 for the first window, beacuse of the padding
        XX_clip = np.clip(XX, clip_value, -clip_value)
        XX_corr = np.correlate(XX, XX_clip, mode='full')
        center = np.argmax(XX_corr)
        right_XX_corr = XX_corr[center:]
        prev_window = max([window - 1, 0])
        if voiced_unvoiced[prev_window] > 0:
            # Want it to be harder to turn off than turn on
            strength_factor = .29
        else:
            strength_factor = .3
        start = np.where(right_XX_corr < corr_limit * XX_corr[center])[0]
        # 20 is hardcoded but should depend on samplerate?
        if start.shape[0] == 0:
            # fix
            start0 = 0
        else:
            start0 = start[0]
        start = np.amax([20, start0])
        search_corr = right_XX_corr[start:]
        if search_corr.shape[0] <= 0:
            #fix
            second_max = 0.0
            index = 0
        else:
            index = np.argmax(search_corr)
            second_max = search_corr[index]
        if (second_max > strength_factor * XX_corr[center]):
            voiced_unvoiced[window] = 1
            period[window] = start + index - 1
        else:
            voiced_unvoiced[window] = 0
            period[window] = 0
    return np.array(voiced_unvoiced), np.array(period)
    
    
#
# AED (Adaptive energy detection) based VAD (Voice activity detection)
#
# from: https://github.com/antoniorohit/CS289A/blob/master/Final%20Project/VAD/aed.py
#

def aeDetect(raw_signal, sensitivity=1.0, region_size = 8000):
    """ Adaptive energy detection
    see:        http://homepage.tudelft.nl/w5p50/pdffiles/VAD%20Techniques%20for%20Real-Time%20Speech%20Transmission%20on%20the%20Internet.pdf 
    :return: cleaned signal
    """
#     offset = prm.params["offset"].get()
    offset = np.argmin(abs(raw_signal))
    #region_size = 880
    var = np.var(raw_signal)
    
    if((offset+region_size/2. < len(raw_signal)) and (offset-region_size/2. > 0)):
        SIGNAL_THRESH = sensitivity*1.5*np.average(abs(raw_signal[offset-region_size/2.:offset + region_size/2.]))
#         print "Thresh: ", SIGNAL_THRESH, offset  
#         print "Var:    ", np.var(raw_signal) 
#         print "Len:    ", len(raw_signal)     
    else:
        SIGNAL_THRESH = sensitivity*100.0
  
    p = 0.1
    old_var = 1.0
        
    # initialize a new array of 16bit integers - the wav file is 16bit
    cleaned_signal = scipy.zeros(len(raw_signal), scipy.int16)
                
    # Go through the wav file in chunks of aed_params["region_size"].get()
    # check if the region is a 'zero region', and if so, clear the signal to 0
    # in that region
    
    half_region = int(region_size/2.)
    rsLen = len(raw_signal)
        
    # start i from aed_params["region_size"].get()/2 so that we dont under or overflow array limits    
    i = half_region
    region=2*half_region
        
    while i <= (rsLen - half_region):
        i1 = i - half_region
        i2 = i + half_region
        signal_of_interest = (raw_signal[i1:i2])
        average_local = np.average(abs(signal_of_interest))
        if average_local < SIGNAL_THRESH:
            SIGNAL_THRESH = (1-p)*SIGNAL_THRESH + 1.5*p*average_local*sensitivity
            cleaned_signal[i1:i2] = scipy.zeros(region, scipy.int16)
            new_var = np.var(signal_of_interest)
            if(new_var/old_var > 1.25):
                p = 0.25
            else:
                p = 0.1
    
            old_var = new_var
        else:
            cleaned_signal[i1:i2] = signal_of_interest
        i += region
    
    # get rid of zero regions
    #cleaned_signal = cleaned_signal[np.nonzero(cleaned_signal)]
    
    if len(cleaned_signal) * 2 < len(raw_signal):
        return []
    
    return cleaned_signal
    
    
