from pyAudioAnalysis import audioTrainTest as aT
from pyAudioAnalysis import audioFeatureExtraction as aF
from scipy.io import wavfile
import math
from collections import OrderedDict
import numpy as np

no_Winner = "---------"
STRINCTNESS = 0.75

def classifyRawAudioChunkDataWithSVM(data, Fs, Classifier, MEAN, STD, classNames, mtWin, mtStep, stWin, stStep, computeBEAT):
    modelType = 'svm'
    [MidTermFeatures, s] = aF.mtFeatureExtraction(data, Fs, mtWin * Fs, mtStep * Fs, round(Fs * stWin), round(Fs * stStep))
    MidTermFeatures = MidTermFeatures.mean(axis=1)        # long term averaging of mid-term statistics
    if computeBEAT:
        [beat, beatConf] = aF.beatExtraction(s, stStep)
        MidTermFeatures = numpy.append(MidTermFeatures, beat)
        MidTermFeatures = numpy.append(MidTermFeatures, beatConf)
    curFV = (MidTermFeatures - MEAN) / STD                # normalization

    [Result, P] = aT.classifierWrapper(Classifier, modelType, curFV)    # classification        
    return Result, P, classNames

def printResult(Result, P, classNames):
    print "{0:s}\t{1:s}".format("Class", "Probability")
    for i, c in enumerate(classNames):
        print "{0:s}\t{1:.2f}".format(c, P[i])
    print "Winner class: " + classNames[int(Result)]
    
def classifyRawAudioDataWithSVM(wavFile, windowSize, svmModel):
    [Classifier, MEAN, STD, classNames, mtWin, mtStep, stWin, stStep, computeBEAT] = aT.loadSVModel(svmModel)
    Fs, data = wavfile.read(wavFile)
    classification = []
    idx = 0
    while idx+windowSize < len(data):
        dataChunk = data[idx:idx+windowSize]
        if all(v == 0 for v in dataChunk):
            winner = no_Winner
        else:
            R, p, classes = classifyRawAudioChunkDataWithSVM(dataChunk, Fs, Classifier, MEAN, STD, classNames, mtWin, mtStep, stWin, stStep, computeBEAT)
            winner = classes[int(R)] if p[int(R)] > STRINCTNESS else no_Winner
        
        classification.append(winner)
        idx = idx + windowSize
        
    return createScoreForClasses(classification, classNames)
    
def createScoreForClasses(classification, classNames):
    BASE = 2
    strengthList = []
    d = {}
    for name in classNames:
        d[name] = {"score": 0, "strength": 0}
    for classif in classification:
        for k in d:
            if classif == k:
                # print "found " + k
                score = d[k]["score"]+1
                #strength = 0 if score == 0 else math.floor(math.log(score, BASE))
                strength = 3 if d[k]["strength"] == 3 else d[k]["strength"]+1
                d[k] = {"score" : score, "strength": strength}
            else:
                # print "not found " + k
                strength = 0 if d[k]["strength"] == 0 else d[k]["strength"]-1
                #score = 0 if strength < 1 else math.pow(BASE, strength)
                score = 0 if d[k]["score"]==0 or strength == 0 else d[k]["score"]-1
                d[k] = {"score" : score, "strength": strength}
        classesDict = OrderedDict(reversed(sorted(d.items(), key=lambda t: t[1]["strength"])))
        #print classesDict
        strengthList.append((classesDict.items()[0][0], classesDict.items()[0][1]["strength"], classesDict.items()[0][1]["score"]))
    return strengthList