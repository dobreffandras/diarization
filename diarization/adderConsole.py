import numpy as np
import matplotlib.pyplot as plt
import sys
from speechRecognizer.adder import add_lower_and_uppermouth

filepath1 = sys.argv[1]
filepath2 = sys.argv[2]
outfilepath = sys.argv[3]

file1 = open(filepath1,"r")
file2 = open(filepath2,"r")


array1 = [[float(x) for x in line.split()] for line in file1]
array2 = [[float(x) for x in line.split()] for line in file2]

output_data = add_lower_and_uppermouth(array1[0], array2[0])

np.savetxt(outfilepath, output_data, "%s ", " ", '')
