import numpy as np
import sys
from speechRecognizer.speechrecognizer import speech_recognize

filepath1 = sys.argv[1]
outfilepath = sys.argv[2]

file1 = open(filepath1,"r")

array1 = [[float(x) for x in line.split()] for line in file1]
t = np.linspace(0, len(array1[0]), len(array1[0]))

data = array1[0]

FPS = 30
output_data = speech_recognize(data, FPS/3, 0.7)
        
np.savetxt(outfilepath, output_data, "%s ", " ", '')
