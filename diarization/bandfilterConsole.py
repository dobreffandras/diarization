import numpy as np
import matplotlib.pyplot as plt
from speechRecognizer.bandfilter import cheby1_bandpass_filter, cheby1_bandpass
import argparse
from scipy.signal import freqz

parser = argparse.ArgumentParser()
parser.add_argument("inputfile", help="The file from the data is read")
parser.add_argument("outputfile", help="The file where the filtered data is written")
parser.add_argument("-v", "--verbose", help="If true the frequency response will be plotted", default=False, type=bool)
parser.add_argument("-o", "--order", help="increase output verbosity", default=1, type=int)
parser.add_argument("-fs", "--samplerate", help="The sample rate of the data", default=44100.0, type=float)
parser.add_argument("-low", "--low", help="The desired cutoff low frequency of the filter, Hz", default=200.0, type=float)
parser.add_argument("-high", "--high", help="The desired cutoff high frequency of the filter, Hz", default=200.0, type=float)
parser.add_argument("-r", "--ripple", help="The ripple dB", default=2.0, type=float)
parser.add_argument("-t", "--type", help="Specifies if high or low", default='low')

args = parser.parse_args()

infilepath = args.inputfile
outfilepath = args.outputfile


# Filter requirements.
order = args.order
fs = args.samplerate      # sample rate, Hz
low = args.low  # desired cutoff low frequency of the filter, Hz
high = args.high  # desired cutoff high frequency of the filter, Hz
ripple = args.ripple # ripple dB

infile = open(infilepath,"r")
array = [[float(x) for x in line.split()] for line in infile]

y = cheby1_bandpass_filter(array[0], ripple,  low, high, fs, order)


output_data = []

for i in range(0, len(y)) :
	output_data.append(y[i])

np.savetxt(outfilepath, output_data, "%s ", " ", '')


if args.verbose:
    # Get the filter coefficients so we can check its frequency response.
    b, a = cheby1_bandpass(low, high, ripple, fs, order)

    # Plot the frequency response.
    w, h = freqz(b, a, worN=8000)

    plt.plot((fs * 0.5 / np.pi) * w, abs(h), label="order = %d" % order)
    
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Gain')
    plt.grid(True)
    plt.legend(loc='best')
    plt.show()