import numpy as np

def speech_recognize(data, windowsize, sensitivity):
    output_data = []

    for i in range(0, len(data), windowsize):
        chunk = data[i : i+windowsize]
        dev = np.std(chunk)
        if dev > sensitivity :
            output_data.extend([1.0]*windowsize)
        else:
            output_data.extend([0.0]*windowsize)
    return output_data