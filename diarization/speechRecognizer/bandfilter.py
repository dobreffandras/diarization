import numpy as np
from scipy.signal import filtfilt, lfilter, freqz, cheby1
import matplotlib.pyplot as plt

def cheby1_bandpass(lowcut, highcut, ripple, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = cheby1(order, ripple, [low, high], btype='band')
    return b, a


def cheby1_bandpass_filter(data, ripple, lowcut, highcut, fs, order=5):
    b, a = cheby1_bandpass(lowcut, highcut, ripple, fs, order=order)
    y = filtfilt(b, a, data)
    return y