import os

def cutUpVideo(inputVideo, segmentsfile, segmentsName = "output"):
    file = open(segmentsfile,"r")
    segments = [[int(x) for x in line.split()] for line in file]

    for i in range(0, len(segments)):
        segment = segments[i]
        duration = (segment[2]*60+segment[3])-(segment[0]*60+segment[1])
        cmd = "ffmpeg -i %s -ss 00:%02d:%02d -t %d %s%d.mp4" % (inputVideo, segment[0], segment[1], duration, segmentsName, i)
        print cmd
        os.system(cmd)


