import numpy as np
from scipy.signal import freqz, cheby1, butter, bessel
import matplotlib.pyplot as plt

def cheby1_bandpass(lowcut, highcut, ripple, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = cheby1(order, ripple, [low, high], btype='band')
    return b, a
    
    

def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a
    

def bessel_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = bessel(order, [low, high], btype='band')
    return b, a
    
def plot(b, a, fs):
    # Plot the frequency response.
    w, h = freqz(b, a, worN=8000)

    plt.plot((fs * 0.5 / np.pi) * w, abs(h))
    
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Gain')
    plt.grid(True)
    plt.legend(loc='best')
    plt.show()
    
b, a = cheby1_bandpass(2, 6, 0.01, 30, 7)
#plot(b, a, 30)
b, a = butter_bandpass(2, 6, 30, 7)
#plot(b, a, 30)
b, a = bessel_bandpass(2, 6, 30, 18)
plot(b, a, 30)