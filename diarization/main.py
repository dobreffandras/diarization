import argparse
import matplotlib.pyplot as plt
from speechRecognizer.bandfilter import cheby1_bandpass_filter
from speechRecognizer.adder import add_lower_and_uppermouth
from speechRecognizer.speechrecognizer import speech_recognize
from voicing.audio_tools import aeDetect
from wavSeparator import create_wav_chunks
from scipy.io import wavfile
import numpy as np
import time

parser = argparse.ArgumentParser()
parser.add_argument("lowermouth", help="The file containing the raw lowermouth data")
parser.add_argument("uppermouth", help="The file containing the raw uppermouth data")
parser.add_argument("audiofile", help="The audiofile (Wav)")
parser.add_argument("fps", help="The fps of the data", type=int)
parser.add_argument("--verbose", help = "Creates additional files holding partial results", action="store_true")

args = parser.parse_args()

#Filter settings
ripple = 0.01
low = 2
high = 6
fs = args.fps
order = 7

#Speech recognition settings
FPS = args.fps
windowsize = FPS/3
speech_sensitivity = 0.7

#vad
vad_sensitivity = 1.5
vad_region_size = 880

def loadData(filePath):
    File = open(filePath,"r")
    array = [[float(x) for x in line.split()] for line in File]
    return array[0]

def speechRecognize(lowermouth, uppermouth):
    lowermouthData_filtered = cheby1_bandpass_filter(lowermouthData, ripple,  low, high, fs, order)
    uppermouthData_filtered = cheby1_bandpass_filter(uppermouthData, ripple,  low, high, fs, order)

    addedData = add_lower_and_uppermouth(lowermouthData_filtered, uppermouthData_filtered)
   
    if args.verbose:
        writeDataToFile(lowermouthData_filtered, "low_filteredData.txt")
        writeDataToFile(uppermouthData_filtered, "upp_filteredData.txt")
        writeDataToFile(addedData, "filteredData.txt")
    
    
    speechData = speech_recognize(addedData, windowsize, speech_sensitivity)
    return speechData
    
def voiceActivityRecognize(wavData):
    clearedWavData = aeDetect(wavData, vad_sensitivity, vad_region_size)
    return clearedWavData
    
def speechVoiceFilterPipeline(speechData, wavData, FPS, wavSampleRate):
    wavFPSRatio = wavSampleRate/FPS
    filteredWavData = np.zeros(len(wavData), dtype=wavData.dtype)
    for i in range(0, len(wavData)):
        wavSignal = wavData[i]
        if i/wavFPSRatio < len(speechData) and speechData[i/wavFPSRatio] > 0.0 :
            filteredWavData[i] = wavSignal
        
    return filteredWavData

def plotData(data, filename):
    plt.figure(num=None, figsize=(100,3), dpi=500)
    plt.plot(np.linspace(0, len(data), len(data)), data)
    plt.savefig(filename)
    plt.close()
    
def writeDataToFile(data, filename):
    np.savetxt(filename, data, "%s ", " ", '')

# -------------------- mouth --------------------
lowermouthData = loadData(args.lowermouth)
uppermouthData = loadData(args.uppermouth)
speechData = speechRecognize(lowermouthData, uppermouthData)

if args.verbose:
    writeDataToFile(speechData, "speech.txt")

# -------------------- audio --------------------
wavSampleRate, wavData = wavfile.read(args.audiofile)
if args.verbose:
    plotData(wavData, "raw.png")

voiceData = voiceActivityRecognize(wavData)
if args.verbose:
    plotData(voiceData, "VAD.png")
    wavfile.write("vad.wav", wavSampleRate, voiceData)
    
# --- filter wav data according to speechData ---
filteredVoiceData = speechVoiceFilterPipeline(speechData, voiceData, FPS, wavSampleRate)
if args.verbose:
    plotData(filteredVoiceData, "filtered_voice.png")

wavfile.write("filtered_voice.wav", wavSampleRate, filteredVoiceData)

# ---------------- create wavChunks -------------
wavWindowSize = int(float(windowsize)/float(FPS)*wavSampleRate)
filteredWavChunks = create_wav_chunks(filteredVoiceData, wavWindowSize)
i = 0
for chunk in filteredWavChunks:
    wavfile.write("%d_%d.wav" % (int(time.time()*1000.0), i), wavSampleRate, chunk)
    i += 1
